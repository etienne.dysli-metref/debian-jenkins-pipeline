//Example code to remotely load the Debian packaging pipeline
@Library('debian-jenkins-pipeline') _
node {
  checkout([$class: 'GitSCM', branches: [[name: '*/master']], userRemoteConfigs: [[credentialsId: '6b7322de-8e55-4108-b362-c8261ac4d310', url: 'git@gitlab.switch.ch:etienne.dysli-metref/debian-jenkins-pipeline.git']]])
  load 'DebianPackagingPipeline.groovy'
}
