def srcdir = 'package_source'
def vendor = env.BRANCH_NAME.split('/')[1]
def distribution = env.BRANCH_NAME.split('/')[2]

stage('Build packages') {
        node('debian') {
          def architecture = 'amd64'
          deleteSourceDirectory(srcdir)
          echo("Distribution from branch name: ${distribution}")
          checkoutAndBuildPackageInDirectory(srcdir, distribution, architecture, '')
          sh 'ls -la'
          testPackage(vendor, distribution, architecture)
          uploadPackage(architecture)
          archiveArtifacts artifacts: "*.dsc,*.debian.tar.?z,*.orig.tar.?z,*_${architecture}.build,*_${architecture}.changes,*_${architecture}.deb,*_all.deb", excludes: null, fingerprint: true
        }
        node('debian') {
          def architecture = 'i386'
          deleteSourceDirectory(srcdir)
          echo("Distribution from branch name: ${distribution}")
          checkoutAndBuildPackageInDirectory(srcdir, distribution, architecture, '--binary-arch')
          sh 'ls -la'
          testPackage(vendor, distribution, architecture)
          uploadPackage(architecture)
          archiveArtifacts artifacts: "*_${architecture}.build,*_${architecture}.changes,*_${architecture}.deb", excludes: null, fingerprint: true
        }
}

def deleteSourceDirectory(srcdir) {
  step([$class: 'WsCleanup', patterns: [[pattern: srcdir, type: 'EXCLUDE']]])
}

def checkoutAndBuildPackageInDirectory(srcdir, distribution, architecture, gitPbuilderOptions) {
  def gbpIgnore = '--git-ignore-branch'
  dir(srcdir) {
    checkout scm
    if (updateChangelog()) {
      gbpIgnore += ' --git-ignore-new'
    }
    sh "gbp buildpackage ${gbpIgnore} --git-pbuilder --git-dist=${distribution} --git-arch=${architecture} --git-pbuilder-autoconf --git-pbuilder-options=\"--configfile /data/aai/etc/pbuilder/pbuilderrc.${distribution} ${gitPbuilderOptions}\" -j4 -sa"
  }
}

def updateChangelog() {
  def changesDetected = sh(returnStatus: true, script: 'gbp dch --ignore-branch --auto | grep "No changes detected"') == 1
  sh 'git checkout -- debian/changelog'
  if (changesDetected) {
    sh "gbp dch --ignore-branch --auto --snapshot --snapshot-number=${env.BUILD_NUMBER}"
  }
  changesDetected
}

def testPackage(vendor, distribution, architecture) {
  sh "sudo piuparts -b /var/cache/pbuilder/base-${distribution}-${architecture}.tgz --keep-sources-list --distribution=${distribution} --arch=${architecture} --bindmount=/data/aai/debarchiver -I 'data/aai/debarchiver/.*' -D ${vendor} *_${architecture}.changes"
}

def uploadPackage(architecture) {
  sh "sudo -g aai dput --config /data/aai/etc/dput.cf local-noscan *_${architecture}.changes"
  sh 'sudo -g aai /data/aai/bin/debarchiver --configfile /data/aai/etc/debarchiver.conf --scandetect'
}
