@Library('debian-jenkins-pipeline') _

def distribution = [
  vendor: vendorFrom(env.BRANCH_NAME),
  codeName: codeNameFrom(env.BRANCH_NAME)
]

pipeline {
  agent none
  stages {
    stage('Build package amd64') {
      agent { label 'debian' }
      environment {
        architecture = 'amd64'
      }
      steps {
        buildPackage(distribution, env.architecture)
        testPackage(distribution, env.architecture, '1'.equals(env.PIUPARTS_USE_BACKPORTS))
        uploadPackage(env.architecture)
      }
      post {
        success {
          archiveArchitectureIndependentArtifacts()
          archiveArchitectureDependentArtifacts(env.architecture)
        }
        always {
          indexRepository(distribution)
        }
      }
    }
    stage('Build package i386') {
      when {
        not { environment name: 'DISABLE_i386', value: '1' }
      }
      agent { label 'debian' }
      environment {
        architecture = 'i386'
      }
      steps {
        buildPackage(distribution, env.architecture, '--binary-arch')
        testPackage(distribution, env.architecture, '1'.equals(env.PIUPARTS_USE_BACKPORTS))
        uploadPackage(env.architecture)
      }
      post {
        success {
          archiveArchitectureDependentArtifacts(env.architecture)
        }
        always {
          indexRepository(distribution)
        }
      }
    }
  }
}
