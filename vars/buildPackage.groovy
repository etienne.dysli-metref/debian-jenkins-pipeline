def call(Map distribution, String architecture, String gitPbuilderOptions = '') {
  String srcdir = 'package_source'
  deleteSourceDirectory(srcdir)
  echo("Building for: ${distribution.vendor} ${distribution.codeName} ${architecture}")
  checkoutAndBuildPackageInDirectory(srcdir, distribution.codeName, architecture, gitPbuilderOptions)
  sh 'ls -la'
}

def deleteSourceDirectory(String srcdir) {
  step([$class: 'WsCleanup', patterns: [[pattern: srcdir, type: 'EXCLUDE']]])
}

def checkoutAndBuildPackageInDirectory(String srcdir, String codeName, String architecture, String gitPbuilderOptions) {
  def gbpIgnore = '--git-ignore-branch'
  dir(srcdir) {
    checkout([
	$class: 'GitSCM',
	branches: scm.branches,
	doGenerateSubmoduleConfigurations: scm.doGenerateSubmoduleConfigurations,
	extensions: [[$class: 'CloneOption', noTags: false, shallow: false, depth: 0, reference: ''], [$class: 'LocalBranch']],
	submoduleCfg: [],
	userRemoteConfigs: scm.userRemoteConfigs
      ])
    sh "git checkout pristine-tar"
    sh "git checkout -"
    if (updateChangelog()) {
      gbpIgnore += ' --git-ignore-new'
    }
    sh "gbp buildpackage ${gbpIgnore} --git-pbuilder --git-dist=${codeName} --git-arch=${architecture} --git-pbuilder-autoconf --git-pbuilder-options=\"--configfile /data/aai/etc/pbuilder/pbuilderrc.${codeName} ${gitPbuilderOptions}\" -j4 -sa"
  }
}

def updateChangelog() {
  def changesDetected = sh(returnStatus: true, script: 'gbp dch --ignore-branch --auto | grep "No changes detected"') == 1
  sh 'git checkout -- debian/changelog'
  if (changesDetected) {
    sh "gbp dch --ignore-branch --auto --snapshot --snapshot-number=${env.BUILD_NUMBER}"
  }
  changesDetected
}
