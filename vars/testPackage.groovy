def call(Map distribution, String architecture, boolean useBackports) {
  testPackage(distribution.vendor, distribution.codeName, architecture, useBackports)
}

def testPackage(String vendor, String codeName, String architecture, boolean useBackports) {
  def distribution = codeName
  if (useBackports) {
    distribution += '-backports'
  }
  //sh "sudo piuparts -b /var/cache/pbuilder/base-${codeName}-${architecture}.tgz --keep-sources-list --distribution=${distribution} --arch=${architecture} --bindmount=/data/aai/debarchiver -I 'data/aai/debarchiver/.*' --no-upgrade-test -D ${vendor} *_${architecture}.changes"
}
