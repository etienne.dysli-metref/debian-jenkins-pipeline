def call(Map distribution) {
  sh 'sudo -g aai /data/aai/bin/debarchiver --configfile /data/aai/etc/debarchiver.conf --scandetect'
  dir("/data/aai/debarchiver/dists/${distribution.codeName}") {
    sh 'rm --force --verbose InRelease'
    sh 'sudo -g aai gpg --homedir /data/aai/etc/gnupg --batch --local-user 27171D22 --clearsign --output InRelease Release'
  }
}
