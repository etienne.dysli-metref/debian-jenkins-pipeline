def call(String architecture) {
  uploadPackage(architecture)
}

def uploadPackage(String architecture) {
  sh "sudo -g aai dput --config /data/aai/etc/dput.cf local-noscan *_${architecture}.changes"
}
