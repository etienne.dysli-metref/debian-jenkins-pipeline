def call() {
  archiveArtifacts artifacts: "*.dsc,*.debian.tar.?z,*.orig.tar.?z,*_all.deb", excludes: null, fingerprint: true
}
