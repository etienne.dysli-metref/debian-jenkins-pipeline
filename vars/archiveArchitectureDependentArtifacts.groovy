def call(String architecture) {
  archiveArtifacts artifacts: "*_${architecture}.build,*_${architecture}.changes,*_${architecture}.deb", excludes: null, fingerprint: true
}
